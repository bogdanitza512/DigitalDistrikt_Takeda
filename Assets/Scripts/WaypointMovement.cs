﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using BN512.Extensions;
using DG.Tweening;

/// <summary>
/// 
/// </summary>
public class WaypointMovement : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

    #region Nested Types

    public enum InteractionPhase { NoInteraction, BeginDrag, Dragging, EndDrag }

    #endregion

    #region Fields and Properties

    public Canvas parentCanvas;

    public WaypointManager waypointManager;

    public InteractionPhase interactionPhase;

    public float waypointDistanceThreshold = .5f;

    public float lerpMovementDuration = .5f;

    float initialWaypointScreenDistance;

    Vector3 nextWaypointScreenPosition;

    float lastComputedAlpha = 0;


    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void OnEnable()
    {
        transform.DOMove(waypointManager.PlayerStart.transform.position,
                         lerpMovementDuration);
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {


    }

    #endregion

    #region Methods

    void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
    {
        if(interactionPhase == InteractionPhase.NoInteraction &&
           (waypointManager.areWaypointsAvailable == true))
        {
            interactionPhase = InteractionPhase.BeginDrag;
            ComputeDragData();
        }
    }

    void IDragHandler.OnDrag(PointerEventData eventData)
    {
        if(interactionPhase == InteractionPhase.BeginDrag)
            interactionPhase = InteractionPhase.Dragging;
        
        if(interactionPhase == InteractionPhase.Dragging &&
           (waypointManager.areWaypointsAvailable == true))
        {
            if (CheckIfWaypointReached())
            {
                transform.DOMove(waypointManager.NextWaypoint.transform.position,
                                 lerpMovementDuration/2);
                waypointManager.CheckInAtNextWaypoint();

                ComputeDragData();

                if (waypointManager.areWaypointsAvailable == false)
                    interactionPhase = InteractionPhase.NoInteraction;
            }

            transform.position = 
                Vector3.Lerp(waypointManager.CurrentWaypoint.transform.position,
                             waypointManager.NextWaypoint.transform.position,
                             ComputeLerpMovementAlpha());
        }

    }

    void IEndDragHandler.OnEndDrag(PointerEventData eventData)
    {
        if(interactionPhase == InteractionPhase.Dragging)
        {
            interactionPhase = InteractionPhase.EndDrag;

            if(CheckIfWaypointReached())
            {
                transform.DOMove(waypointManager.NextWaypoint.transform.position,
                                 lerpMovementDuration / 2);
                waypointManager.CheckInAtNextWaypoint();

            }
            else
            {
                transform.DOMove(waypointManager.LastReliableWaypoint.transform.position,
                                 lerpMovementDuration);
                waypointManager.ReturnToLastReliableWaypoint();
            }

            interactionPhase = InteractionPhase.NoInteraction;
        }
    }

    private void ComputeDragData()
    {
        lastComputedAlpha = 0;

        nextWaypointScreenPosition =
            RectTransformUtility.WorldToScreenPoint(parentCanvas.worldCamera,
                                                    waypointManager.NextWaypoint.transform.position);
        print("nextWaypointScreenPosition : {0}".FormatWith(nextWaypointScreenPosition));


        initialWaypointScreenDistance = 
            Vector2.Distance(Input.mousePosition,
                             nextWaypointScreenPosition);
        print("initialWaypointScreenDistance : {0}".FormatWith(initialWaypointScreenDistance));

    }

    private float ComputeLerpMovementAlpha()
    {
        var nextWaypointScreenDistance =
            Vector2.Distance(Input.mousePosition, nextWaypointScreenPosition);
        /*
        var newComputedAlpha =
            1 - Mathf.Clamp01(nextWaypointScreenDistance /
                              initialWaypointScreenDistance);
        if(newComputedAlpha + waypointDistanceThreshold/2 < lastComputedAlpha)
        {
            lastComputedAlpha = 0;
            transform.DOMove(waypointManager.LastReliableWaypoint.transform.position,
                             lerpMovementDuration / 2);
            waypointManager.ReturnToLastReliableWaypoint();

        }
        else
        {
            lastComputedAlpha = newComputedAlpha;
        }
        */
        ///*
        lastComputedAlpha =
            Mathf.Max(lastComputedAlpha,
                      1 - Mathf.Clamp01(nextWaypointScreenDistance /
                                        initialWaypointScreenDistance));
        //*/
        print("lastComputedAlpha : {0}".FormatWith(lastComputedAlpha));
        return lastComputedAlpha;
    }

    private bool CheckIfWaypointReached()
    {
        var playerWaypointWorldDistance =
            Vector3.Distance(transform.position,
                             waypointManager.NextWaypoint.transform.position);
        var thresholdModifier =
            waypointManager.NextWaypoint.distanceThresholdModifier;

        return (playerWaypointWorldDistance <
                (waypointDistanceThreshold * thresholdModifier));
    }

    public void ResetMovement()
    {
        transform.position = waypointManager.PlayerStart.transform.position;
    }

    #endregion

}
