﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// 
/// </summary>
public class MiniGameController : MonoBehaviour {

    #region Nested Types

    #endregion

    #region Fields and Properties

    public UnityEvent onResetMiniGame;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

	public void ResetMiniGame()
    {
        onResetMiniGame.Invoke();
    }

    #endregion

}
