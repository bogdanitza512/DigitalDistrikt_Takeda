﻿using System.Collections;
using System.Collections.Generic;
using BN512.Extensions;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// 
/// </summary>
public class PuzzlePiece : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
    

    #region Nested Types

    public enum InteractionPhase { NoInteraction, BeginDrag, Dragging, EndDrag }

    #endregion

    #region Fields and Properties

    public PuzzleManager puzzleManager;

    public InteractionPhase interactionPhase;

    public bool isInFinalPosition = false;

    public float thresholdMultiplier = 1.0f;

    Vector3 initalPos
    {
        get{
            return puzzleManager.pieceTargetDict[this].initialPos;
        }
    }

    Vector3 targetPos
    {
        get
        {
            return puzzleManager.pieceTargetDict[this].targetPos;
        }
    }

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    public void AutoPopulate()
    {
        //puzzlePieceCanvas = GetComponent<Canvas>();
    }

    void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
    {
        if(interactionPhase == InteractionPhase.NoInteraction &&
           isInFinalPosition == false)
        {
            interactionPhase = InteractionPhase.BeginDrag;
            transform.SetSiblingIndex(puzzleManager.pieceTargetDict.Count - 1);
        }
    }

    void IDragHandler.OnDrag(PointerEventData eventData)
    {
        if (interactionPhase == InteractionPhase.BeginDrag)
            interactionPhase = InteractionPhase.Dragging;
        if(interactionPhase == InteractionPhase.Dragging)
        {
            var mouseScreenPos = 
                Input.mousePosition.WithZ(puzzleManager.masterCanvas.planeDistance);
            var mouseWorldPos =
                puzzleManager.masterCanvas.worldCamera.ScreenToWorldPoint(mouseScreenPos);
            
            transform.position = mouseWorldPos;

            if(CheckIfNearTarget(thresholdMultiplier * 
                                 puzzleManager.thresholdTargetDistance))
            {
                LockToTargetPos();
            }
        }
    }

    private void LockToTargetPos()
    {
        transform.DOMove(targetPos, puzzleManager.tweenDuration / 2);
        isInFinalPosition = true;
        puzzleManager.CompletedPieces++;
        interactionPhase = InteractionPhase.NoInteraction;
    }

    void IEndDragHandler.OnEndDrag(PointerEventData eventData)
    {
        if(interactionPhase == InteractionPhase.Dragging)
        {
            interactionPhase = InteractionPhase.EndDrag;

            if (CheckIfNearTarget(thresholdMultiplier *
                                 puzzleManager.thresholdTargetDistance))
            {
                LockToTargetPos();
            }
            else
            {
                transform.DOMove(initalPos, puzzleManager.tweenDuration);
            }

            interactionPhase = InteractionPhase.NoInteraction;
        }
    }

    bool CheckIfNearTarget(float threshold)
    {
        return Vector3.Distance(transform.position, targetPos) < threshold;
    }


    public void ResetPuzzlePiece()
    {
        isInFinalPosition = false;
        transform.position = initalPos;
    }

    #endregion

}
