﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using DG.Tweening;

/// <summary>
/// 
/// </summary>
public class MonumentWaypoint : Waypoint {

    #region Nested Types



    #endregion

    #region Fields and Properties

    [SerializeField]
    Transform monumentParent;

    [SerializeField]
    Image shadow;

    [SerializeField]
    Image color;

    [SerializeField]
    float activationDuration = 1.0f;

    bool AreFieldsValid
    {
        get{
            return monumentParent != null &&
                   shadow != null &&
                   color != null;
        }
    }

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        if (!AreFieldsValid)
            AutoPopulateFields();
        color.fillAmount = 0;

    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    void AutoPopulateFields()
    {
        foreach(Transform child in transform)
        {
            if(child.name == "[Monument]")
            {
                monumentParent = child;
            }
        }

        if (monumentParent != null)
        {
            foreach (Transform child in monumentParent)
            {
                if (child.name == "Image_Shadow")
                {
                    shadow = child.GetComponent<Image>();
                }
                if (child.name == "Image_Color")
                {
                    color = child.GetComponent<Image>();
                }
            }
        }


    }

    public override void OnWaypointReached()
    {
        base.OnWaypointReached();

        DOTween.Sequence()
               .Append(monumentParent.DOScale(1.25f, activationDuration / 2))
               .Append(color.DOFillAmount(1.0f, activationDuration))
               .Join(monumentParent.DOScale(1,activationDuration/2));
    }

    public override void Reset()
    {
        base.Reset();
        color.fillAmount = 0;
    }

    #endregion

}
