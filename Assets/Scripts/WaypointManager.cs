﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

/// <summary>
/// 
/// </summary>
public class WaypointManager : MonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public bool areWaypointsAvailable = true;

    [SerializeField]
    MazeScoreboard scoreboard;

    [SerializeField]
    int lastReliableWaypointIndex = 0;

    [SerializeField]
    int currentWaypointIndex = 0;

    [SerializeField]
    List<Waypoint> waypoints;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        if (waypoints == null || waypoints.Count == 0)
            AutoPopulate();
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

	[Button(ButtonSizes.Medium)]
    void AutoPopulate()
    {
        waypoints.Clear();
        foreach(Transform child in transform)
        {
            var aux = child.GetComponent<Waypoint>();
            if(aux != null)
            {
                waypoints.Add(aux);
            }
        }
    }


    public Waypoint LastReliableWaypoint
    {
        get{
            return waypoints[lastReliableWaypointIndex];
        }
    }

    public Waypoint CurrentWaypoint
    {
        get{
            return waypoints[currentWaypointIndex];
        }
    }

    public Waypoint NextWaypoint
    {
        get{
            return waypoints[GetNextValidWaypointIndex()];
        }
    }

    public Waypoint PlayerStart 
    {
        get
        {
            return waypoints[0];
        }
    }

    public void CheckInAtNextWaypoint()
    {
        currentWaypointIndex = GetNextValidWaypointIndex();

        if(waypoints[currentWaypointIndex].isReliable == true)
        {
            lastReliableWaypointIndex = currentWaypointIndex;
            scoreboard.AddPoints(waypoints[currentWaypointIndex].rewardPoints);
            waypoints[currentWaypointIndex].OnWaypointReached();
        }
    }

    int GetNextValidWaypointIndex()
    {
        if(currentWaypointIndex == waypoints.Count - 1)
        {
            areWaypointsAvailable = false;
            return currentWaypointIndex;
        }
        else 
        {
            return currentWaypointIndex + 1;
        }
    }

    public void ReturnToLastReliableWaypoint()
    {
        currentWaypointIndex = lastReliableWaypointIndex;
    }

    public void ResetMaze()
    {
        currentWaypointIndex = 0;
        areWaypointsAvailable = true;
        lastReliableWaypointIndex = 0;

        foreach(var waypoint in waypoints)
        {
            waypoint.Reset();
        }
    }

    #endregion

}
