﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using System;

/// <summary>
/// 
/// </summary>
public class Waypoint : MonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public bool isReliable;
    public int rewardPoints = 10;
    public bool hasBeenReached = false;
    public float distanceThresholdModifier = 1;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    public virtual void OnWaypointReached()
    {
        
    }

    public virtual void Reset()
    {
        hasBeenReached = false;
    }

    #endregion

}
