﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using ScreenMgr;
using System;
using DG.Tweening;

/// <summary>
/// 
/// </summary>
public class PuzzleManager : SerializedMonoBehaviour {

    #region Nested Types

    [System.Serializable]
    public struct PuzzlePieceSpecs
    {
        [SerializeField]
        public Vector3 initialPos;

        [SerializeField]
        public Vector3 targetPos;
    }

    #endregion

    #region Fields and Properties

    [SerializeField]
    Transform puzzlePieceParent;

    [SerializeField]
    Transform puzzleTargetParent;

    public Canvas masterCanvas;

    public float thresholdTargetDistance = .5f;

    public Dictionary<PuzzlePiece, PuzzlePieceSpecs> pieceTargetDict =
        new Dictionary<PuzzlePiece, PuzzlePieceSpecs>();

    [SerializeField]
    int completedPieces;

    [SerializeField]
    ScreenManager puzzleHUD;

    [SerializeField]
    BaseScreen nextDestinationScreen;

    public int CompletedPieces
    {
        get{
            return completedPieces;
        }
        set{
            completedPieces = value;
            if(value >= pieceTargetDict.Count)
            {
                DOTween.Sequence()
                       .AppendInterval(tweenDuration)
                       .Append(transform.DOScale(1.25f, tweenDuration * 2))
                       .OnComplete(
                           ()=> 
                            { 
                                puzzleHUD.Show(nextDestinationScreen); 
                            });
            }
        }
    }

    public float tweenDuration = .5f;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        if(pieceTargetDict.Count == 0)
        {
            AutoPopulate();
        }
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    void AutoPopulate()
    {
        pieceTargetDict.Clear();
        foreach(Transform puzzleChild in puzzlePieceParent)
        {
            var puzzlePiece = 
                puzzleChild.GetComponent<PuzzlePiece>();
            if(puzzleChild != null)
            {
                foreach(Transform targetChild in puzzleTargetParent)
                {
                    if(puzzleChild.name == targetChild.name)
                    {
                        pieceTargetDict.Add(puzzlePiece,
                                            new PuzzlePieceSpecs
                                            {
                                                initialPos = puzzlePiece.transform.position,
                                                targetPos = targetChild.position
                                            });
                        puzzlePiece.puzzleManager = this;
                        puzzlePiece.AutoPopulate();
                    }
                }
            }


        }
    }

    public void ResetPuzzle()
    {
        foreach(var piece in pieceTargetDict.Keys)
        {
            piece.ResetPuzzlePiece();
            transform.localScale = 
                Vector3.one;
            puzzleHUD.HideAll();
            CompletedPieces = 0;
        }
    }

    #endregion

}
