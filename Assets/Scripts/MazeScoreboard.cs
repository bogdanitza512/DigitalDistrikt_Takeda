﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using TMPro;
using BN512.Extensions;
using ScreenMgr;

/// <summary>
/// 
/// </summary>
public class MazeScoreboard : MonoBehaviour {


    #region Nested Types



    #endregion

    #region Fields and Properties

    [SerializeField]
    ScreenManager screenManager;

    [SerializeField]
    BaseScreen welcomeDialogScreen;

    [SerializeField]
    BaseScreen scoreCounterScreen;

    [SerializeField]
    TMP_Text scoreText;

    [SerializeField]
    string scoreMsgTemplate;

    [SerializeField]
    BaseScreen gameOverScreen;

    [SerializeField]
    TMP_Text gameOverText;

    [SerializeField]
    string gameOverMsgTemplate;

    bool isWelcomeDialogVisible = false;

    bool isScoreCounterVisible = false;

    bool isGameOverScreenVisible = false;


    public int currentScore = 0;

    public int maxScore = 100;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        if(scoreText == null || gameOverText == null)
        {
            AutoPopulate();
        }
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }



    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    void AutoPopulate()
    {
        foreach(Transform child in scoreCounterScreen.transform)
        {
            if(child.name == "Text_Score")
            {
                scoreText = child.GetComponent<TMP_Text>();
            }
        }

        foreach (Transform child in gameOverScreen.transform)
        {
            if (child.name == "Text_GameOver")
            {
                gameOverText = child.GetComponent<TMP_Text>();
            }
        }
    }

    public void ShowWelcomeDialog()
    {
        if(!isWelcomeDialogVisible)
        {
            screenManager.Show(welcomeDialogScreen);
            isWelcomeDialogVisible = true;
        }
    }

    public void ShowScoreCounterOver(int score)
    {
        if (!isScoreCounterVisible)
        {
            screenManager.Show(scoreCounterScreen);
            isScoreCounterVisible = true;
        }
        scoreText.text = scoreMsgTemplate.FormatWith(score);

    }

    public void ShowGameOver(int score)
    {
        if (!isGameOverScreenVisible)
        {
            screenManager.Show(gameOverScreen);
            isGameOverScreenVisible = true;
        }
        gameOverText.text = gameOverMsgTemplate.FormatWith(score);
    }

    public void AddPoints(int rewardPoints)
    {
        currentScore += rewardPoints;

        ShowScoreCounterOver(currentScore);

        if (currentScore >= maxScore)
        {
            ShowGameOver(currentScore);   
        }
            
    }




    public void ResetScore()
    {
        currentScore = 0;

        scoreText.text 
                 = scoreMsgTemplate.FormatWith(currentScore);
        gameOverText.text
                 = scoreMsgTemplate.FormatWith(currentScore);

        isWelcomeDialogVisible = false;
        isScoreCounterVisible = false;
        isGameOverScreenVisible = false;


        screenManager.HideAll();
    }

    #endregion

}
